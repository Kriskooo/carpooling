﻿using AutoMapper;
using CarpoolingProject.Data;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.Exceptions;
using CarpoolingProject.Services.Interfaces;
using CarpoolingProject.Services.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class UserService : IUserService
    {
        private readonly CarpoolingContext context;
        private readonly IMapper mapper;

        public UserService(CarpoolingContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<User> GetUserAsync(int id)

        {
            var user = await context.Users.Include(x => x.RatingsForUser).Include(x => x.CreatedRatings).FirstOrDefaultAsync(x => x.UserId == id);
            return user ?? throw new EntityNotFoundException();
        }

        public int UserCount()
        {
            var users = context.Users.Count();
            return users;
        }

        public bool Exist(string email)
        {
            return context.Users.Any(x => x.Email.Equals(email));
        }

        public async Task<User> GetUsernameAsync(string username)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.UserName == username);
            return user ?? throw new EntityNotFoundException();
        }

        public async Task<IEnumerable<UserDto>> GetAllPassengersAsync()
        {
            var passengers = await context.Users.Select(x => mapper.Map<UserDto>(x)).ToListAsync();
            return passengers;
        }

        public async Task<InfoResponseModel> CreateUserAsync(CreateUserRequestModel requestModel)
        {
            var passwordHasher = new PasswordHasher<User>();


            var responseModel = new InfoResponseModel();

            if (requestModel.UserName == null ||
                requestModel.Password == null ||
                requestModel.FirstName == null ||
                requestModel.LastName == null ||
                requestModel.Email == null ||
                requestModel.PhoneNumber == null
                )
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.USER_WRONG_PARAMETERS;
                return responseModel;
            }

            var user = new User()
            {
                UserName = requestModel.UserName,
                FirstName = requestModel.FirstName,
                LastName = requestModel.LastName,
                Email = requestModel.Email,
                PhoneNumber = requestModel.PhoneNumber,
                StarsCount = 0,
                TravelCountAsDriver = 0,
                ReviewCount = 0
            };
            var userRole = new UserRole
            {
                UserId = user.UserId,
                RoleId = 3
            };


            if (await context.Users.AnyAsync(x => x.UserName == requestModel.UserName))
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.USERNAME_ALREADY_EXIST;

                return responseModel;
            }

            if (await context.Users.AnyAsync(e => e.Email == requestModel.Email))
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.EMAIL_ALREADY_EXIST;

                return responseModel;
            }
            user.PasswordHash = passwordHasher.HashPassword(user, requestModel.Password);

            user.Roles.Add(userRole);
            context.Users.Add(user);

            await context.SaveChangesAsync();

            responseModel.IsSuccess = true;
            responseModel.Message = Constants.USER_CREATE_SUCCESS;

            return responseModel;
        }

        public async Task<InfoResponseModel> DeleteUserAsync(DeleteUserRequestModel requestModel)
        {
            var responseModel = new InfoResponseModel();
            var user = await context.Users.FirstOrDefaultAsync(u => u.UserId == requestModel.Id);

            if (user == null)
            {
                responseModel.Message = Constants.USER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }

            else
            {
                responseModel.Message = Constants.USER_DELETED;
                responseModel.IsSuccess = true;

                this.context.Users.Remove(user);
                await this.context.SaveChangesAsync();
            }
            return responseModel;
        }

        public async Task<InfoResponseModel> UpdateUserAsync(UpdateUserRequestModel requestModel)
        {
            var responseModel = new InfoResponseModel();

            if (requestModel.UserName == null ||
                requestModel.Password == null ||
                requestModel.FirstName == null ||
                requestModel.LastName == null ||
                requestModel.Email == null ||
                requestModel.PhoneNumber == null ||
                requestModel.ConfirmPassword == null
                )
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.USER_WRONG_PARAMETERS;
                return responseModel;
            }
            var user = await GetUserAsync(requestModel.Id);

            if (user != null)
            {
                if (requestModel.UserName != user.UserName)
                {

                    if (await context.Users.AnyAsync(x => x.UserName == requestModel.UserName))
                    {
                        responseModel.IsSuccess = false;
                        responseModel.Message = Constants.USERNAME_ALREADY_EXIST;
                        return responseModel;
                    }
                    user.UserName = requestModel.UserName;
                }

                if (requestModel.Password != user.PasswordHash)
                {
                    user.PasswordHash = requestModel.Password;
                }

                if (requestModel.FirstName != user.FirstName)
                {
                    user.FirstName = requestModel.FirstName;
                }

                if (requestModel.LastName != user.LastName)
                {
                    user.LastName = requestModel.LastName;
                }

                if (requestModel.Email != user.Email)
                {
                    if (await context.Users.AnyAsync(e => e.Email == requestModel.Email))
                    {
                        responseModel.IsSuccess = false;
                        responseModel.Message = Constants.EMAIL_ALREADY_EXIST;
                        return responseModel;
                    }
                    user.Email = requestModel.Email;
                }

                if (requestModel.PhoneNumber != user.PhoneNumber)
                {
                    user.PhoneNumber = requestModel.PhoneNumber;
                    responseModel.Message = Constants.USER_WRONG_PARAMETERS;
                    responseModel.IsSuccess = false;
                    return responseModel;
                }
                if (requestModel.ImagePath != user.ImagePath)
                {
                    user.ImagePath = user.ImagePath;
                }

                await context.SaveChangesAsync();
                responseModel.Message = Constants.USER_CREATE_SUCCESS + $"{user.UserId}";
                responseModel.IsSuccess = true;
            }
            else
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.USER_UPDATE_ERROR + $"{user.UserId} id";
            }
            return responseModel;
        }

        public async Task<IEnumerable<UserDto>> FilterUsersAsync(SearchUserRequestModel requestModel)
        {
            var users = context.Users.AsQueryable();

            if (!string.IsNullOrEmpty(requestModel.Username))
            {
                users = users.Where(u => u.UserName.Contains(requestModel.Username));
            }

            if (!string.IsNullOrEmpty(requestModel.Email))
            {
                users = users.Where(u => u.Email.Contains(requestModel.Email));
            }

            if (!string.IsNullOrEmpty(requestModel.PhoneNumber))
            {
                users = users.Where(u => u.Email.Contains(requestModel.PhoneNumber));
            }

            if (!string.IsNullOrEmpty(requestModel.CheckBlocked))
            {
                users = users.Where(u => !u.IsBlocked);
            }

            var result = await users.Select(u => mapper.Map<UserDto>(u)).ToListAsync();

            return result;
        }
    }
}

