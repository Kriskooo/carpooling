﻿using AutoMapper;
using CarpoolingProject.Data;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class DriverService : IDriverService
    {
        private readonly CarpoolingContext context;
        private readonly ITravelService travelService;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public DriverService(CarpoolingContext context, ITravelService travelService, IUserService userService, IMapper mapper)
        {
            this.context = context;
            this.travelService = travelService;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<DriverDto>> GetAllDriversAsync()
        {
            var drivers = await context.Users.Where(x => x.Roles.Any(r => r.Role.Name == "Driver")).Select(x => mapper.Map<DriverDto>(x)).ToListAsync();
            return drivers;
        }
        public async Task<IEnumerable<UserDto>> GetAllPassengersForTripAsync(GetTravelRequestModel requestModel)
        {
            var travel = await travelService.GetTravelAsync(requestModel.TravelId);

            //var travel = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.TravelId);
            var passengerIds = new List<int>();
            foreach (var passenger in travel.Passengers)
            {
                passengerIds.Add(passenger.PassengerId);
            }

            var travelPassengers = new List<UserDto>();

            foreach (var passengerId in passengerIds)
            {
                var userDto = mapper.Map<UserDto>(await userService.GetUserAsync(passengerId));
                travelPassengers.Add(userDto);
            }
            await context.SaveChangesAsync();
            return travelPassengers;
        }
        public async Task<InfoResponseModel> SelectUserFromApplication(SelectPassengerRequestModel requestModel)
        {
            var response = new InfoResponseModel();

            var selectedPassenger = await userService.GetUserAsync(requestModel.PassengerId);
            var travel = await travelService.GetTravelAsync(requestModel.TravelId);
            var applicant = await context.TravelApplications.FirstOrDefaultAsync(x => x.ApplicantId == requestModel.PassengerId);

            if (selectedPassenger != null && travel != null)
            {
                var travelPassenger = new TravelPassenger
                {
                    PassengerId = selectedPassenger.UserId,
                    TravelId = travel.TravelId
                };

                if (requestModel.Liked)
                {
                    response.Message = $"You liked ";
                    travel.Passengers.Add(travelPassenger);
                    //travel.ApplicantsForTravel.Remove(applicant);
                    applicant.IsAccepted = true;
                    await context.SaveChangesAsync();
                    return response;
                }

                applicant.IsAccepted = false;
                await context.SaveChangesAsync();

                response.Message = $"You didn't like {selectedPassenger.UserId}";
                response.IsSuccess = true;
                return response;
            }
            response.Message = "Invalid info";
            response.IsSuccess = false;
            return response;
        }

        public async Task<InfoResponseModel> RemovePassenger(SelectPassengerRequestModel requestModel)
        {
            var response = new InfoResponseModel();

            var travel = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.TravelId);
            var passenger = travel.Passengers.FirstOrDefault(x => x.PassengerId == requestModel.PassengerId);

            var time = (travel.DepartureTime - DateTime.Now).TotalHours;

            if (time > 1)
            {
                passenger.IsDeclined = true;
                travel.Status = "Removed from trip";
                travel.Passengers.Remove(passenger);
                response.IsSuccess = true;
                response.Message = "Passenger successfully removed";
                return response;
            }
            response.IsSuccess = false;
            response.Message = "Can't remove passenger from trip 1 hour before the trip";
            return response;
        }
    }
}
