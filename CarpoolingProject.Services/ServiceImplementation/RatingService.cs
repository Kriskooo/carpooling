﻿using AutoMapper;
using CarpoolingProject.Data;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.Interfaces;
using CarpoolingProject.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class RatingService : IRatingService
    {
        private readonly CarpoolingContext context;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IDriverService driverService;
        private readonly ITravelService travelService;

        public RatingService(CarpoolingContext context, IMapper mapper, IUserService userService, IDriverService driverService, ITravelService travelService)
        {
            this.context = context;
            this.mapper = mapper;
            this.userService = userService;
            this.driverService = driverService;
            this.travelService = travelService;
        }
        public async Task<Review> GetRating(int id)
        {
            var rating = await context.Ratings.FirstOrDefaultAsync(x => x.ReviewId == id);
            return rating;
        }
        public async Task<InfoResponseModel> DriverRating(ReviewRequestModel requestModel)
        {
            var response = new InfoResponseModel();
            var travel = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.TravelId);
            var passenger = travel.Passengers.First();

            if (travel.IsFinished)
            {
                if (requestModel.Stars >= 1 && requestModel.Stars <= 5)
                {
                    var driver = await userService.GetUserAsync(travel.CreatorId);
                    if (passenger.PassengerId != travel.CreatorId)
                    {
                        var rating = new Review
                        {
                            RatedUserId = travel.CreatorId,
                            AuthorId = passenger.PassengerId,
                            Description = requestModel.Description,
                            StarsCount = requestModel.Stars,
                            CreatedOn = DateTime.Now
                        };

                        var userRating = await userService.GetUserAsync(passenger.PassengerId);
                        userRating.CreatedRatings.Add(rating);
                        context.Ratings.Add(rating);

                        travel.Passengers.Remove(passenger);
                        driver.RatingsForUser.Add(rating);

                        await context.SaveChangesAsync();
                        response.IsSuccess = true;
                        response.Message = "Review Successfully added!";

                        return response;
                    }
                }
                response.IsSuccess = false;
                response.Message = Constants.INVALID_PARAMS;

                return response;
            }

            response.IsSuccess = false;
            response.Message = "Can't leave rating while trip is still in progress";

            return response;
        }
        public async Task<IEnumerable<RatingDto>> UserRatings(GetUserRequestModel requestModel)
        {
            var user = await userService.GetUserAsync(requestModel.Id);
            var ratings = user.RatingsForUser.Where(x => x.RatedUserId == requestModel.Id).Select(x => mapper.Map<RatingDto>(x)).OrderByDescending(x => x.CreatedOn).ToList();
            return ratings;
        }

        public async Task<IEnumerable<RatingDto>> AllRatings()
        {
            var ratings = await context.Ratings.Select(x => mapper.Map<RatingDto>(x)).ToListAsync();
            return ratings;
        }

        public async Task<InfoResponseModel> PassengerRating(ReviewRequestModel requestModel)
        {
            var response = new InfoResponseModel();

            var travel = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.TravelId);
            var passenger = travel.Passengers.First();

            var driver = await userService.GetUserAsync(travel.CreatorId);
            var ratedUser = await userService.GetUserAsync(passenger.PassengerId);

            if (travel.IsFinished)
            {
                if (requestModel.Stars >= 1 && requestModel.Stars <= 5)
                {

                    if (passenger.PassengerId != travel.CreatorId)
                    {
                        var rating = new Review
                        {
                            RatedUserId = passenger.PassengerId,
                            AuthorId = travel.CreatorId,
                            Description = requestModel.Description,
                            StarsCount = requestModel.Stars,
                            CreatedOn = DateTime.Now
                        };

                        driver.CreatedRatings.Add(rating);
                        context.Ratings.Add(rating);

                        travel.Passengers.Remove(passenger);
                        ratedUser.RatingsForUser.Add(rating);
                        await context.SaveChangesAsync();

                        response.IsSuccess = true;
                        response.Message = "Review Successfully added!";

                        return response;
                    }
                }
                response.IsSuccess = false;
                response.Message = Constants.INVALID_PARAMS;
                return response;
            }
            response.IsSuccess = false;
            response.Message = "Can't leave rating while trip is still in progress";

            return response;
        }
        public async Task<InfoResponseModel> DeleteReview(GetUserRequestModel requestModel)
        {
            var response = new InfoResponseModel();
            var review = await GetRating(requestModel.Id);
            if (review != null)
            {
                review.isDeleted = true;
                await context.SaveChangesAsync();

                response.IsSuccess = true;
                response.Message = "Successfully deleted";

                return response;
            }
            response.IsSuccess = true;
            response.Message = "Review does not exist";

            return response;
        }
        public async Task<InfoResponseModel> UpdateReview(UpdateReviewRequestModel requestModel)
        {
            var response = new InfoResponseModel();
            var review = await GetRating(requestModel.ReviewId);

            if (review != null && requestModel.Stars >= 1 && requestModel.Stars <= 5)
            {
                if (review.Description != requestModel.Description || review.StarsCount != requestModel.Stars)
                {
                    review.IsChanged = true;
                    review.LastChangedOn = DateTime.Now;
                }

                review.Description = requestModel.Description;
                review.StarsCount = requestModel.Stars;
            }
            if (review.IsChanged)
            {
                response.Message = "Review changed successfully.";
                response.IsSuccess = true;

                return response;
            }
            response.Message = "Nothing is changed.";
            response.IsSuccess = false;

            return response;
        }
    }
}
