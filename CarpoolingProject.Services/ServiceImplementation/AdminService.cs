﻿using CarpoolingProject.Data;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.Interfaces;
using CarpoolingProject.Services.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class AdminService : IAdminService
    {
        private readonly CarpoolingContext context;
        private readonly IUserService userService;
        private readonly IDriverService driverService;
        private readonly ITravelService travelService;

        public AdminService(CarpoolingContext context, IUserService userService, IDriverService driverService, ITravelService travelService)
        {
            this.context = context;
            this.userService = userService;
            this.driverService = driverService;
            this.travelService = travelService;
        }

        public async Task<IEnumerable<DriverDto>> GetAllDriversAsync()
        {
            var drivers = await driverService.GetAllDriversAsync();
            return drivers;
        }

        public async Task<IEnumerable<TravelDto>> GetAllTravelsAsync()
        {
            var travels = await travelService.GetAllTravelsAsync();
            return travels;
        }

        public async Task<IEnumerable<UserDto>> GetAllPassengersAsync()
        {
            var passengers = await userService.GetAllPassengersAsync();
            return passengers;
        }

        public async Task<InfoResponseModel> DeleteUserAsync(DeleteUserRequestModel requestModel)
        {
            var responseModel = new InfoResponseModel();
            var user = await context.Users.FirstOrDefaultAsync(u => u.UserId == requestModel.Id);

            if (user == null)
            {
                responseModel.Message = Constants.USER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }

            else
            {
                responseModel.Message = Constants.USER_DELETED;
                responseModel.IsSuccess = true;

                this.context.Users.Remove(user);
                await this.context.SaveChangesAsync();
            }
            return responseModel;
        }

        public async Task<InfoResponseModel> ChangeRoleAsync(ChangeRoleRequestModel requestModel)
        {
            var responeModel = new InfoResponseModel();
            var user = await userService.GetUserAsync(requestModel.UserId);

            var userRole = await context.UserRoles.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            userRole.RoleId = requestModel.RoleId;
            await context.SaveChangesAsync();
            responeModel.Message = "test";
            return responeModel;
        }

        public async Task<InfoResponseModel> BlockAsync(int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.UserId == id);
            var responseModel = new InfoResponseModel();

            if (user == null)
            {
                responseModel.Message = Constants.USER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }
            else
            {
                responseModel.IsSuccess = true;
                user.IsBlocked = true;
                await this.context.SaveChangesAsync();
                responseModel.Message = Constants.SUCCESS_BLOCKED;
            }
            return responseModel;
        }

        public async Task<InfoResponseModel> UnBlockAsync(int id)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.UserId == id);
            var responseModel = new InfoResponseModel();

            if (user == null)
            {
                responseModel.Message = Constants.USER_NOT_FOUND;
                responseModel.IsSuccess = false;
            }
            else
            {
                responseModel.IsSuccess = true;
                user.IsBlocked = false;
                await this.context.SaveChangesAsync();
                responseModel.Message = Constants.SUCCESS_UNBLOCKED;
            }
            return responseModel;
        }
    }
}
