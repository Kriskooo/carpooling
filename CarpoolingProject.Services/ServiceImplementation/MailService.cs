﻿using CarpoolingProject.Services.Interfaces;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class MailService : IMailService
    {
        private readonly IUserService userService;
        public MailService(IUserService userService)
        {
            this.userService = userService;
        }
        public async Task MailVerificationAsync(string userName)
        {
            var user = await userService.GetUsernameAsync(userName);
            var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("carpooling@abv.bg", "Example User");
            var subject = $"Carpool {user.UserName} Validation";
            var to = new EmailAddress($"{user.Email}", $"{user.UserName}");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>http://localhost:5000/User/Login</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}