﻿using AutoMapper;
using CarpoolingProject.Data;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.Exceptions;
using CarpoolingProject.Services.Interfaces;
using CarpoolingProject.Services.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.ServiceImplementation
{
    public class TravelService : ITravelService
    {
        private readonly CarpoolingContext context;
        private readonly ICityService cityService;
        private readonly IAddressService addressService;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public SerializationInfo ErrorMassage { get; private set; }

        public TravelService(CarpoolingContext context, ICityService cityService, IAddressService addressService, IUserService userService, IMapper mapper)
        {
            this.context = context;
            this.cityService = cityService;
            this.addressService = addressService;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<Travel> GetTravelAsync(int id)
        {
            var travel = await context.Travels
                .Include(x => x.ApplicantsForTravel)
                .Include(x => x.Passengers)
                .Include(x => x.Creator)
                .Where(x => x.IsDeleted == false)
               .FirstOrDefaultAsync(x => x.TravelId == id);

            return travel ?? throw new EntityNotFoundException();

        }
        public int FinishedTravels()
        {
            var travels = context.Travels.Where(x => x.IsFinished == true).Count();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> GetAllTravelsAsync()
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                               .Include(x => x.StartPoint)
                                                    .ThenInclude(x => x.City)
                                               .Include(x => x.EndPoint)
                                                    .ThenInclude(x => x.City)
                                                .Where(x => x.IsFinished == false)
                                               .Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> GetAllTravelsAsync(int id)
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                               .Include(x => x.StartPoint)
                                                    .ThenInclude(x => x.City)
                                               .Include(x => x.EndPoint)
                                                    .ThenInclude(x => x.City)
                                                .Where(x => x.IsFinished == true && (x.CreatorId == id || x.Passengers.Any(x => x.PassengerId == id)))
                                               .Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FutureTravelsASync(int id)
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                               .Include(x => x.StartPoint)
                                                    .ThenInclude(x => x.City)
                                               .Include(x => x.EndPoint)
                                                    .ThenInclude(x => x.City)
                                                .Where(x => x.IsFinished == false && x.CreatorId == id)
                                               .Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public int TravelCount()
        {
            var travelsCount = this.context.Travels.Where(x => x.IsFinished == true).Count();
            return travelsCount;
        }

        public async Task<InfoResponseModel> CreateTravelAsync(CreateTravelRequestModel requestModel)
        {
            var cityRequestModel = new CreateCityRequestModel();
            var responseModel = new InfoResponseModel();
            if (requestModel.CreatorId < 1 ||
                //requestModel.StartPoint == null ||
                //requestModel.EndPoint == null ||
                requestModel.FreeSpots < 1
                )
            {
                responseModel.IsSuccess = false;
                responseModel.Message = Constants.INVALID_PARAMS;
                return responseModel;
            }

            //checks if city context contains city
            var start = context.Cities.Any(x => x.Name == requestModel.StartCity);
            var end = context.Cities.Any(x => x.Name == requestModel.EndCity);
            //------------------------------------------------------------------------
            //creates new travel
            var creator = context.Users.FirstOrDefault(x => x.UserId == requestModel.CreatorId);
            var role = await context.UserRoles.FirstOrDefaultAsync(x => x.UserId == creator.UserId);
            role.RoleId = 2;
            //requestModel.Creator = creator;
            var travel = new Travel()
            {
                CreatorId = requestModel.CreatorId,
                Creator = creator,
                //StartPoint = requestModel.StartPoint,
                //EndPoint = requestModel.EndPoint,
                DepartureTime = requestModel.DepartureTime,
                FreeSpots = requestModel.FreeSpots,
                PricePerPeson = requestModel.PricePerPerson,
                AdditionalInfo = requestModel.AdditionalInfo,
            };
            if (travel.AdditionalInfo == null)
            {
                travel.AdditionalInfo = "no restrictions";
            }
            if (start)
            {
                var city = context.Cities.FirstOrDefault(x => x.Name == requestModel.StartCity);
                var address = new Address()
                {
                    // City = city,
                    CityId = city.CityId,
                    StreetName = requestModel.StartStreet
                };
                travel.StartPoint = address;
            }
            else
            {

                cityRequestModel.Name = requestModel.StartCity;
                cityRequestModel.CountryId = 1;
                var create = await cityService.CreateCityAsync(cityRequestModel);

                var city = this.context.Cities.FirstOrDefault(x => x.Name == requestModel.StartCity);

                var address = new Address()
                {
                    City = city,
                    CityId = city.CityId,
                    StreetName = requestModel.StartStreet
                };
                travel.StartPoint = address;
            }
            if (end)
            {
                var city = context.Cities.FirstOrDefault(x => x.Name == requestModel.EndCity);
                var address = new Address()
                {
                    City = city,
                    CityId = city.CityId,
                    StreetName = requestModel.EndStreet
                };
                travel.EndPoint = address;
            }
            else
            {
                cityRequestModel.Name = requestModel.EndCity;
                cityRequestModel.CountryId = 1;
                var create = await cityService.CreateCityAsync(cityRequestModel);

                var city = this.context.Cities.FirstOrDefault(x => x.Name == requestModel.EndCity);
                var address = new Address()
                {
                    City = city,
                    CityId = city.CityId,
                    StreetName = requestModel.EndStreet
                };
                travel.EndPoint = address;
            }
            context.Travels.Add(travel);
            await context.SaveChangesAsync();

            responseModel.IsSuccess = true;
            responseModel.Message = Constants.TRAVEL_CREATE_SUCCESS;

            return responseModel;

        }
        public async Task<InfoResponseModel> DeleteTravelAsync(DeleteTravelRequestModel requestModel)
        {
            var responseModel = new InfoResponseModel();
            var travelToDelete = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.Id);

            if (travelToDelete == null)
            {
                responseModel.Message = Constants.TRAVEL_NOT_FOUND;
                responseModel.IsSuccess = false;
            }
            else
            {
                responseModel.Message = Constants.TRAVEL_DELETE_SUCCESS;
                responseModel.IsSuccess = true;
                this.context.Travels.Remove(travelToDelete);
                this.context.SaveChanges();
            }
            return responseModel;

        }

        public async Task<InfoResponseModel> UpdateTravelAsync(UpdateTravelRequestModel requestModel)
        {
            var passwordHassher = new PasswordHasher<User>();
            var response = new InfoResponseModel();
            if (requestModel.CreatorId < 1 ||
                requestModel.FreeSpots < 1)
            //requestModel.EndPoint == null ||
            //requestModel.StartPoint == null

            {
                response.IsSuccess = false;
                response.Message = Constants.INVALID_PARAMS;
                return response;
            }

            var travel = await context.Travels.FirstOrDefaultAsync(x => x.TravelId == requestModel.TravelId);

            if (travel != null && requestModel.CreatorId == travel.CreatorId)
            {
                if (requestModel.PricePerPerson != travel.PricePerPeson)
                {
                    travel.PricePerPeson = requestModel.PricePerPerson;
                }
                else if (requestModel.FreeSpots != travel.FreeSpots)
                {
                    travel.FreeSpots = requestModel.FreeSpots;
                }
                else if (requestModel.AdditionalInfo != travel.AdditionalInfo)
                {
                    travel.AdditionalInfo = requestModel.AdditionalInfo;
                }
                else if (requestModel.StartCity != travel.StartPoint.City.Name)
                {
                    var addressToDelete = await context.Addresses.FirstOrDefaultAsync(x => x.StreetName == travel.StartPoint.StreetName && x.City.Name == travel.StartPoint.City.Name);

                    context.Addresses.Remove(addressToDelete);

                    await context.SaveChangesAsync();

                    var city = new City
                    {
                        Name = requestModel.StartCity,
                        CountryId = 1
                    };
                    var address = new Address
                    {
                        CityId = city.CityId,
                        City = city,
                        StreetName = requestModel.StartStreet
                    };
                }
                else if (requestModel.EndCity != travel.EndPoint.City.Name)
                {
                    var addressToDelete = await context.Addresses.FirstOrDefaultAsync(x => x.StreetName == travel.EndPoint.StreetName && x.City.Name == travel.EndPoint.City.Name);
                    var addressToUpdate = await context.Addresses.FirstOrDefaultAsync(x => x.StreetName == travel.EndPoint.StreetName && x.City.Name == travel.EndPoint.City.Name);

                    addressToUpdate.City.Name = requestModel.EndCity;

                    context.Addresses.Remove(addressToDelete);
                    await context.SaveChangesAsync();

                    var city = new City
                    {
                        Name = requestModel.EndCity,
                        CountryId = 1
                    };
                    var address = new Address
                    {
                        CityId = city.CityId,
                        City = city,
                        StreetName = requestModel.EndStreet
                    };
                }
                else if (requestModel.StartStreet != travel.StartPoint.StreetName)
                {
                    var addressToUpdate = await context.Addresses.FirstOrDefaultAsync(x => x.StreetName == travel.StartPoint.StreetName && x.City.Name == travel.StartPoint.City.Name);
                    addressToUpdate.StreetName = requestModel.StartStreet;
                }
                else if (requestModel.EndStreet != travel.StartPoint.StreetName)
                {
                    var addressToUpdate = await context.Addresses.FirstOrDefaultAsync(x => x.StreetName == travel.EndPoint.StreetName && x.City.Name == travel.EndPoint.City.Name);
                    addressToUpdate.StreetName = requestModel.EndStreet;
                }
                //else if (requestModel.EndPoint != travel.EndPoint)
                //{
                //    travel.EndPoint = requestModel.EndPoint;
                //}
                //else if (requestModel.StartPoint != travel.StartPoint)
                //{
                //    travel.StartPoint = requestModel.StartPoint;
                //}
                else if (requestModel.DepartureTime != travel.DepartureTime)
                {
                    var result = (travel.DepartureTime - requestModel.DepartureTime).TotalHours;
                    if (result > 1)
                    {
                        travel.DepartureTime = requestModel.DepartureTime;

                        response.Message = "Successfully changed the time of departure";
                        response.IsSuccess = true;


                        return response;
                    }
                    response.Message = "You cannot change the time of departure if it is below 1 hour";
                    response.IsSuccess = false;

                    return response;
                }
                var password = passwordHassher.VerifyHashedPassword(travel.Creator, travel.Creator.PasswordHash, requestModel.ConfirmPassword);
                if (password != PasswordVerificationResult.Success)
                {
                    response.Message = "Wrong Password";
                    response.IsSuccess = false;

                    return response;
                }
                if (password == PasswordVerificationResult.Success)
                {
                    response.Message = "Success";
                    response.IsSuccess = true;
                }

                //else if (requestModel.Id != travel.TravelId)
                //{
                //    response.Message = Constants.TRAVEL_UNATHORIZED;
                //    response.IsSuccess = false;
                //    return response;
                //}
                await context.SaveChangesAsync();

                response.Message = Constants.TRAVEL_CREATE_SUCCESS + $"{travel.TravelId}";
                response.IsSuccess = true;
            }
            else
            {
                response.IsSuccess = false;
                response.Message = Constants.TRAVEL_UPDATE_ERROR + $"{travel.TravelId} id";
            }
            return response;
        }
        public async Task<InfoResponseModel> FinishedTravel(FinishedTravelRequestModel requestModel)
        {
            var deleteTravel = new DeleteTravelRequestModel();

            var travel = await GetTravelAsync(requestModel.Id);
            var driver = await userService.GetUserAsync(travel.CreatorId);
            var response = new InfoResponseModel();

            deleteTravel.Id = travel.TravelId;

            if (requestModel.IsFinished)
            {
                response.IsSuccess = true;
                response.Message = "Travel finished!";
                driver.TravelCountAsDriver++;
                travel.IsFinished = true;

                await context.SaveChangesAsync();

                return response;
            }
            response.Message = "We are still travelling.";
            return response;
        }
        public async Task<InfoResponseModel> ApplyForTravel(ApplyForTravelRequestModel requestModel)
        {
            var response = new InfoResponseModel();

            var passenger = await userService.GetUserAsync(requestModel.UserId);
            var travel = await GetTravelAsync(requestModel.TravelId);

            if (travel != null)

            {
                if (travel.CreatorId != passenger.UserId)
                {
                    var travelAplication = new TravelApplication
                    {
                        ApplicantId = passenger.UserId,
                        TravelId = travel.TravelId
                    };

                    travel.ApplicantsForTravel.Add(travelAplication);
                    response.IsSuccess = true;

                    response.Message = "Added successfully to list";
                    travel.FreeSpots--;

                    if (travel.FreeSpots <= 0)
                    {
                        response.IsSuccess = false;
                        response.Message = "Sorry seats are full";
                        return response;
                    }
                    await context.SaveChangesAsync();
                    return response;
                }
                response.IsSuccess = false;
                response.Message = "You are already driver for the trip!";

                return response;
            }
            response.IsSuccess = false;
            response.Message = "Failed to add to list";

            return response;
        }
        //Searchs for travels
        public async Task<IEnumerable<TravelDto>> SearchForTravels(SearchForTravelRequestModel requestModel)
        {
            var travels = await context.Travels.Where(x => x.StartPoint.City.Name == requestModel.StartCity &&
                                                      x.EndPoint.City.Name == requestModel.EndCity &&
                                                      x.DepartureTime.Date == requestModel.Date).Select(travel => new TravelDto(travel))
                                                      .ToListAsync();
            var nowDate = DateTime.Now.Date;
            //if no travels with that date return travels with same cities but with date to be in the future

            if (travels == null)
            {
                travels = await context.Travels.Where(x => x.StartPoint.City.Name == requestModel.StartCity &&
                                                      x.EndPoint.City.Name == requestModel.EndCity &&
                                                      x.DepartureTime.Date >= requestModel.Date).Select(travel => new TravelDto(travel))
                                                      .ToListAsync();
            }
            return travels;
        }
        public async Task<IEnumerable<UserDto>> ListApplicantsForTravel(GetTravelRequestModel requestModel)
        {
            var travel = await GetTravelAsync(requestModel.TravelId);
            //var applicants = mapper.Map<ICollection<UserDto>>(travel.ApplicantsForTravel);
            var applicantsIds = new List<int>();

            foreach (var application in travel.ApplicantsForTravel)
            {
                applicantsIds.Add(application.ApplicantId);
            }

            var travelApplicants = new List<UserDto>();

            foreach (var applicatntsId in applicantsIds)
            {
                var userDto = mapper.Map<UserDto>(await userService.GetUserAsync(applicatntsId));
                travelApplicants.Add(userDto);
            }
            return travelApplicants;
        }
        public async Task<IEnumerable<TravelDto>> ListTravelAsPassengerAsync(string email)
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                               .Include(x => x.StartPoint)
                                                .ThenInclude(x => x.City)
                                               .Include(x => x.EndPoint)
                                                .ThenInclude(x => x.City)
                                                .Where(x => x.Creator.Email == email)
                                               .Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> ListTravelAsDriverAsync(int id)
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                               .Include(x => x.StartPoint)
                                                .ThenInclude(x => x.City)
                                               .Include(x => x.EndPoint)
                                                .ThenInclude(x => x.City)
                                                .Where(x => x.Passengers.Any(x => x.PassengerId == id))
                                               .Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByCreator(string search)
        {
            var travels = await context.Travels.Include(x => x.Creator)
                                                    .Include(x => x.StartPoint)
                                                    .ThenInclude(x => x.City)
                                                    .Include(x => x.EndPoint)
                                                    .ThenInclude(x => x.City)
                                            .Where(
                                                x => x.Creator.UserName.Contains(search) ||
                                                x.Creator.Email.Contains(search) ||
                                                x.Creator.PhoneNumber.Contains(search) ||
                                                x.StartPoint.City.Name.Contains(search) ||
                                                x.EndPoint.City.Name.Contains(search))
                                          .Select(travel => new TravelDto(travel)).ToListAsync();

            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByDateDescending(DateTime date)
        {
            var travels = await context.Travels.OrderByDescending(x => DateTime.Compare(x.DepartureTime, date)).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByDateAscending(DateTime date)
        {
            var travels = await context.Travels.OrderBy(x => DateTime.Compare(x.DepartureTime, date)).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByStartTownDescending()
        {
            var travels = await context.Travels.OrderByDescending(x => x.StartPoint.City).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByStartTownAscending()
        {
            var travels = await context.Travels.OrderBy(x => x.StartPoint.City).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByEndTownDescending()
        {
            var travels = await context.Travels.OrderByDescending(x => x.EndPoint.City).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
        public async Task<IEnumerable<TravelDto>> FilterTravelsByEndTownAscending()
        {
            var travels = await context.Travels.OrderBy(x => x.EndPoint.City).Select(travel => new TravelDto(travel)).ToListAsync();
            return travels;
        }
    }
}
