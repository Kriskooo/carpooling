﻿using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface IAdminService
    {
        Task<InfoResponseModel> UnBlockAsync(int id);
        Task<InfoResponseModel> BlockAsync(int id);
        Task<IEnumerable<DriverDto>> GetAllDriversAsync();
        Task<IEnumerable<TravelDto>> GetAllTravelsAsync();
        Task<IEnumerable<UserDto>> GetAllPassengersAsync();
        Task<InfoResponseModel> DeleteUserAsync(DeleteUserRequestModel requestModel);
        Task<InfoResponseModel> ChangeRoleAsync(ChangeRoleRequestModel requestModel);
    }
}
