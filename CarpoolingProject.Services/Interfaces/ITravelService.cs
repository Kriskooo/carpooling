﻿using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface ITravelService
    {
        Task<Travel> GetTravelAsync(int id);
        Task<IEnumerable<TravelDto>> GetAllTravelsAsync();
        Task<IEnumerable<TravelDto>> GetAllTravelsAsync(int id);
        Task<IEnumerable<TravelDto>> FutureTravelsASync(int id);
        int TravelCount();
        Task<InfoResponseModel> CreateTravelAsync(CreateTravelRequestModel requestModel);
        Task<InfoResponseModel> DeleteTravelAsync(DeleteTravelRequestModel requestModel);
        Task<InfoResponseModel> UpdateTravelAsync(UpdateTravelRequestModel requestModel);
        Task<InfoResponseModel> FinishedTravel(FinishedTravelRequestModel requestModel);
        Task<InfoResponseModel> ApplyForTravel(ApplyForTravelRequestModel requestModel);
        Task<IEnumerable<UserDto>> ListApplicantsForTravel(GetTravelRequestModel requestModel);
        Task<IEnumerable<TravelDto>> SearchForTravels(SearchForTravelRequestModel requestModel);
        Task<IEnumerable<TravelDto>> ListTravelAsPassengerAsync(string email);
        Task<IEnumerable<TravelDto>> ListTravelAsDriverAsync(int id);
        Task<IEnumerable<TravelDto>> FilterTravelsByCreator(string search);
        Task<IEnumerable<TravelDto>> FilterTravelsByDateDescending(DateTime date);
        Task<IEnumerable<TravelDto>> FilterTravelsByDateAscending(DateTime date);
        Task<IEnumerable<TravelDto>> FilterTravelsByStartTownDescending();
        Task<IEnumerable<TravelDto>> FilterTravelsByStartTownAscending();
        Task<IEnumerable<TravelDto>> FilterTravelsByEndTownDescending();
        Task<IEnumerable<TravelDto>> FilterTravelsByEndTownAscending();
    }
}