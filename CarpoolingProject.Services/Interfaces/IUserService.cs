﻿using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> FilterUsersAsync(SearchUserRequestModel requestModel);
        bool Exist(string email);
        Task<User> GetUsernameAsync(string username);
        Task<IEnumerable<UserDto>> GetAllPassengersAsync();
        Task<User> GetUserAsync(int id);
        Task<InfoResponseModel> CreateUserAsync(CreateUserRequestModel requestModel);
        Task<InfoResponseModel> DeleteUserAsync(DeleteUserRequestModel requestModel);
        Task<InfoResponseModel> UpdateUserAsync(UpdateUserRequestModel requestModel);
    }
}
