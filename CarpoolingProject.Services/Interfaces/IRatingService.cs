﻿using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface IRatingService
    {
        Task<Review> GetRating(int id);
        Task<InfoResponseModel> DriverRating(ReviewRequestModel requestModel);
        Task<InfoResponseModel> PassengerRating(ReviewRequestModel requestModel);
        Task<InfoResponseModel> DeleteReview(GetUserRequestModel requestModel);
        Task<InfoResponseModel> UpdateReview(UpdateReviewRequestModel requestModel);
        Task<IEnumerable<RatingDto>> UserRatings(GetUserRequestModel requestModel);
    }
}
