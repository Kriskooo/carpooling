﻿using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface ICountryService
    {
        Task<Country> GetCountryAsync(int id);
        Task<InfoResponseModel> CreateCountryAsync(CreateCountryRequestModel requestModel);
        Task<InfoResponseModel> DeleteCountryAsync(DeleteCountryRequestModel requestModel);
        Task<InfoResponseModel> AddCitiesToCountryAsync(AddCitiesToCountryRequestModel requestModel);
        Task<IEnumerable<City>> ShowCitiesAsync(ShowCitiesRequestModel requestModel);
    }
}
