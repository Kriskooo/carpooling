﻿using System.Threading.Tasks;

namespace CarpoolingProject.Services.Interfaces
{
    public interface IMailService
    {
        Task MailVerificationAsync(string userName);
    }
}
