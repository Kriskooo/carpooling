﻿using CarpoolingProject.Data;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.ServiceImplementation;
using CarpoolingProject.Services.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Carpooling.Services.UnitTests
{
    [TestClass]
    public class CountryServiceUnitTests
    {
        [TestMethod]
        public async Task CountryService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Sofia",
                },
                new Country()
                {
                    CountryId = 1,
                    Name = "Varna",
            }  };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var cityService = new CityService(mockContext.Object);
            var service = new CountryService(mockContext.Object, cityService);

            var request = new CreateCountryRequestModel()
            {
                Name = "TEST"
            };

            var result = await service.CreateCountryAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));

            mockContext.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task CountryService_Should_GetCountriesCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country
                {
                    CountryId = 1
                },
                new Country
                {
                    CountryId = 2
                },
                new Country
                {
                    CountryId = 3
                }
            };

            var mockContextSet = mockCountries.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Countries).Returns(mockContextSet.Object);

            var cityService = new CityService(mockContext.Object);
            var service = new CountryService(mockContext.Object, cityService);

            var result = await service.GetCountryAsync(1);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Country));
            Assert.AreEqual(result.CountryId, 1);
        }

        [TestMethod]
        public async Task CountryService_ShouldDelete_WhenParametersCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var cityService = new CityService(mockContext.Object);
            var service = new CountryService(mockContext.Object, cityService);

            var request = new DeleteCountryRequestModel()
            {
                Id = 1
            };

            var result = await service.DeleteCountryAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));

            mockContext.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task CountryService_Should_Delete_WhenParametersInvalid()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);

            var cityService = new CityService(mockContext.Object);
            var service = new CountryService(mockContext.Object, cityService);

            var request = new DeleteCountryRequestModel()
            {
                Id = 100
            };

            var result = await service.DeleteCountryAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Message, Constants.COUNTRY_NULL_ERROR);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));
        }
    }
}

