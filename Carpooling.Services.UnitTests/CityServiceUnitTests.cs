﻿using CarpoolingProject.Data;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Models.ResponseModels;
using CarpoolingProject.Services.ServiceImplementation;
using CarpoolingProject.Services.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockQueryable.Moq;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Carpooling.Services.UnitTests
{
    [TestClass]
    public class CityServiceUnitTests
    {
        [TestMethod]
        public async Task CityService_ShouldCreate_WhenParametersCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    CityId = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesContextSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.Cities).Returns(mockCitiesContextSet.Object);
            mockContext.Setup(context => context.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new CityService(mockContext.Object);

            var request = new CreateCityRequestModel()
            {
                CountryId = 1,
                Name = "TEST"
            };

            var result = await service.CreateCityAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));

            mockContext.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task CityService_ShouldNot_Create_WhenParametersInvalid()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    CityId = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesContextSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.Cities).Returns(mockCitiesContextSet.Object);

            var service = new CityService(mockContext.Object);

            var request = new CreateCityRequestModel()
            {
                CountryId = 1,
                Name = "Sofia"
            };

            var result = await service.CreateCityAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));
        }

        [TestMethod]
        public async Task CityService_Should_GetCitiesCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCities = new List<City>()
            {
                new City
                {
                    CityId = 1
                },
                new City
                {
                    CityId = 2
                },
                new City
                {
                    CityId = 3
                }
            };

            var mockContextSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(db => db.Cities).Returns(mockContextSet.Object);

            var service = new CityService(mockContext.Object);

            var result = await service.GetCityAsync(1);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(City));
            Assert.AreEqual(result.CityId, 1);
        }

        [TestMethod]
        public async Task CityService_ShouldDelete_WhenParametersCorrect()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    CityId = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesContextSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.Cities).Returns(mockCitiesContextSet.Object);
            mockContext.Setup(context => context.SaveChangesAsync(It.IsAny<CancellationToken>())).Verifiable();

            var service = new CityService(mockContext.Object);

            var request = new DeleteCityRequestModel()
            {
                Id = 1
            };

            var result = await service.DeleteCityAsync(request);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));

            mockContext.Verify(db => db.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task CityService_Should_Delete_WhenParametersInvalid()
        {
            var mockContext = new Mock<CarpoolingContext>();

            var mockCountries = new List<Country>()
            {
                new Country()
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                }
            };

            var mockCities = new List<City>()
            {
                new City()
                {
                    CityId = 1,
                    Name = "Sofia",
                    CountryId = 1
                },
                new City()
                {
                    CityId = 2,
                    Name = "Varna",
                    CountryId = 1
            }  };

            var mockCountriesContextSet = mockCountries.AsQueryable().BuildMockDbSet();
            var mockCitiesContextSet = mockCities.AsQueryable().BuildMockDbSet();

            mockContext.Setup(context => context.Countries).Returns(mockCountriesContextSet.Object);
            mockContext.Setup(context => context.Cities).Returns(mockCitiesContextSet.Object);

            var service = new CityService(mockContext.Object);

            var request = new DeleteCityRequestModel()
            {
                Id = 100
            };

            var result = await service.DeleteCityAsync(request);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Message, Constants.CITY_NULL_ERROR);
            Assert.IsInstanceOfType(result, typeof(InfoResponseModel));
        }
    }
}

