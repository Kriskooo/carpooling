﻿using AutoMapper;
using CarpoolingProject.Models.Dtos;
using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Web.Models;

namespace CarpoolingProject.Web.MappingConfiguration
{
    public class CarpoolingProjectProfile : Profile
    {
        public CarpoolingProjectProfile()
        {
            this.CreateMap<User, UserDto>().ReverseMap();
            this.CreateMap<User, DriverDto>().ReverseMap();
            this.CreateMap<Travel, TravelDto>().ReverseMap();
            this.CreateMap<Review, RatingDto>().ReverseMap();
            this.CreateMap<RegisterViewModel, User>().ReverseMap();
            this.CreateMap<TravelApplication, UserDto>().ReverseMap();
            this.CreateMap<UserViewModel, UpdateUserRequestModel>().ReverseMap();
        }
    }
}
