﻿using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminService adminService;
        private readonly IUserService userService;
        public AdminController(IAdminService adminService, IUserService userService)
        {
            this.adminService = adminService;
            this.userService = userService;
        }

        public async Task<IActionResult> ListAllUsers()

        {
            var users = await userService.GetAllPassengersAsync();
            return this.View("Index", users);
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Block(int number)
        {
            var result = await this.adminService.BlockAsync(number);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }
            return this.RedirectToAction("ListAllUsers");
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UnBlock(int number)
        {
            var result = await this.adminService.UnBlockAsync(number);

            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }
            return this.RedirectToAction("ListAllUsers");
        }
    }
}
