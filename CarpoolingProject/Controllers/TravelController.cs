﻿using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers
{
    public class TravelController : Controller
    {
        private readonly ITravelService travelService;
        private readonly IUserService userService;
        private readonly IDriverService driverService;

        public TravelController(ITravelService travelService, IUserService userService, IDriverService driverService)
        {
            this.travelService = travelService;
            this.userService = userService;
            this.driverService = driverService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var travels = await travelService.GetAllTravelsAsync();
            return this.View(travels);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var requestModel = new CreateTravelRequestModel();
            return this.View(requestModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] CreateTravelRequestModel requestModel)
        {
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var creator = await userService.GetUsernameAsync(username);
            if (!this.ModelState.IsValid)
            {
                return this.View(requestModel);
            }
            requestModel.CreatorId = creator.UserId;
            await travelService.CreateTravelAsync(requestModel);
            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Update(int number)
        {
            var requestModel = new UpdateTravelRequestModel();
            requestModel.TravelId = number;
            return this.View(requestModel);
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromForm] UpdateTravelRequestModel requestModel)
        {
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var creator = await userService.GetUsernameAsync(username);
            if (!this.ModelState.IsValid)
            {
                return this.View(requestModel);
            }
            requestModel.CreatorId = creator.UserId;
            await travelService.UpdateTravelAsync(requestModel);
            return this.RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Apply(int number)
        {
            var username = HttpContext.Session.GetString("CurrentUser");
            var passenger = await userService.GetUsernameAsync(username);
            var application = new ApplyForTravelRequestModel();
            application.TravelId = number;
            application.UserId = passenger.UserId;
            await travelService.ApplyForTravel(application);
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Sort(string search)
        {
            var travels = await travelService.FilterTravelsByCreator(search);
            return this.View("Index", travels);
        }

        public async Task<IActionResult> GetAllPassengers(int number)
        {
            var requestModel = new GetTravelRequestModel();
            requestModel.TravelId = number;
            var passengers = await driverService.GetAllPassengersForTripAsync(requestModel);
            return this.View("TravelPassengers_TableView", passengers);
        }

        public async Task<IActionResult> GetAllApplicants(int number)
        {
            var requestModel = new GetTravelRequestModel();
            requestModel.TravelId = number;
            var applicants = await travelService.ListApplicantsForTravel(requestModel);
            return this.View("TravelApplications_TableView", applicants);
        }
        //    public async Task<IActionResult> Accept(int number,int applicant)
        //    {
        //        var requestModel = new SelectPassengerRequestModel();
        //        var travel = await travelService.GetTravelAsync(number);
        //        travel
        //    }
        //}
    }
}
