﻿using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] CreateUserRequestModel requestModel)
        {
            var responce = await userService.CreateUserAsync(requestModel);
            return Ok(responce);
        }

        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromBody] DeleteUserRequestModel requestModel)
        {
            var responce = await userService.DeleteUserAsync(requestModel);
            if (responce.IsSuccess)
            {
                return Ok(responce.Message);
            }
            return BadRequest(responce.Message);
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] UpdateUserRequestModel requestModel)
        {
            var response = await userService.UpdateUserAsync(requestModel);
            return Ok(response);
        }

        [HttpGet("passengers")]
        public async Task<IActionResult> Get()
        {
            var users = await userService.GetAllPassengersAsync();
            return Ok(users);
        }

        [HttpGet("filter")]
        public async Task<IActionResult> Filter([FromQuery] SearchUserRequestModel requestModel)
        {
            var users = await userService.FilterUsersAsync(requestModel);
            return Ok(users);
        }
    }
}
