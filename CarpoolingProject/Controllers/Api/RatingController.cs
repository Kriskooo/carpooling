﻿using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IRatingService ratingService;

        public RatingController(IRatingService ratingService)
        {
            this.ratingService = ratingService;
        }

        [HttpPost("driver")]
        public async Task<IActionResult> ReviewForDriver(ReviewRequestModel requestModel)
        {
            var result = await ratingService.DriverRating(requestModel);
            return this.Ok(result);
        }

        [HttpPost("passenger")]
        public async Task<IActionResult> ReviewForPassenger(ReviewRequestModel requestModel)
        {
            var result = await ratingService.PassengerRating(requestModel);
            return this.Ok(result);
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> DeleteReview(GetUserRequestModel requestModel)
        {
            var result = await ratingService.DeleteReview(requestModel);
            return this.Ok(result);
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateReview(UpdateReviewRequestModel requestModel)
        {
            var result = await ratingService.UpdateReview(requestModel);
            return this.Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> ReviewsForUser(GetUserRequestModel requestModel)
        {
            var result = await ratingService.UserRatings(requestModel);
            return this.Ok(result);
        }
    }
}
