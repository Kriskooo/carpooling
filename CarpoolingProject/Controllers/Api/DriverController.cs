﻿using CarpoolingProject.Models.RequestModels;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverController : ControllerBase
    {
        private readonly IDriverService driverService;

        public DriverController(IDriverService driverService)
        {
            this.driverService = driverService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTravels()
        {
            var result = await driverService.GetAllDriversAsync();
            if (result.Count() < 1)
            {
                return this.NoContent();
            }
            return this.Ok(result);
        }

        [HttpPost("select")]
        public async Task<IActionResult> SelectUser([FromBody] SelectPassengerRequestModel requestModel)
        {
            var result = await driverService.SelectUserFromApplication(requestModel);
            return this.Ok(result);
        }

        [HttpGet("passengers")]
        public async Task<IActionResult> GetAllPassengers([FromBody] GetTravelRequestModel requestModel)
        {
            var result = await driverService.GetAllPassengersForTripAsync(requestModel);
            return this.Ok(result);
        }
    }
}
