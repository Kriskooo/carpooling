﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace CarpoolingProject.Web.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var roles = context.HttpContext.Session.GetString("CurrentRoles").Split(',').ToList();

            if (!roles.Contains(this.Role))
            {
                context.Result = new UnauthorizedResult();
            }
        }
        public string Role { get; set; }
    }
}
