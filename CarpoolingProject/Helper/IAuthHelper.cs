﻿using CarpoolingProject.Models.EntityModels;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Helper
{
    public interface IAuthHelper
    {
        Task<User> TryGetUserAsync(string username);
        Task<User> TryGetUserAsync(string username, string password);
    }
}