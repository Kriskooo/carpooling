﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Helper
{
    public class ExtensionMethod
    {
        public const string urlDevelopment = "../CarpoolingProject.Web/wwwroot/images/";

        public static async Task<bool> UploadImageFile(int id, string type, IFormFile file, bool IsUploadedForTheFirstTime)
        {
            if (file == null)
            {
                if (IsUploadedForTheFirstTime)
                {
                    string sourceFileName = urlDevelopment + $"{type}/default.jpg";
                    string destinationFileName = urlDevelopment + $"{type}/" + id + ".jpg";
                    File.Copy(sourceFileName, destinationFileName, true);
                    return true;
                }
            }
            else
            {
                using (var fileStream = new FileStream(urlDevelopment + $"{type}/" + id + ".jpg", FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    return true;
                }
            }
            return false;
        }
    }
}
