﻿using CarpoolingProject.Models.EntityModels;
using CarpoolingProject.Services.Exceptions;
using CarpoolingProject.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace CarpoolingProject.Web.Helper
{
    public class AuthHelper : IAuthHelper
    {
        private static readonly string ErrorMassage = "Invalid authentication info.";
        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<User> TryGetUserAsync(string username)
        {
            try
            {
                return await userService.GetUsernameAsync(username);
            }
            catch (EntityNotFoundException)
            {
                throw new AuthenticationException(ErrorMassage);
            }
        }

        public async Task<User> TryGetUserAsync(string username, string password)
        {
            var hasher = new PasswordHasher<User>();

            var user = await TryGetUserAsync(username);
            var result = hasher.VerifyHashedPassword(user, user.PasswordHash, password);

            if (result != PasswordVerificationResult.Success)
            {
                throw new AuthenticationException(ErrorMassage);
            }
            return user;
        }
    }
}
