﻿using Microsoft.AspNetCore.Http;

namespace CarpoolingProject.Web.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
