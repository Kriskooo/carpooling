﻿namespace CarpoolingProject.Models.ResponseModels
{
    public class InfoResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
