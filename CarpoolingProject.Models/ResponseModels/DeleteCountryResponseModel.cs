﻿namespace CarpoolingProject.Models.ResponseModels
{
    public class DeleteCountryResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
