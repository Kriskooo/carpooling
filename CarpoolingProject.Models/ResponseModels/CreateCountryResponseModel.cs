﻿namespace CarpoolingProject.Models.ResponseModels
{
    public class CreateCountryResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
