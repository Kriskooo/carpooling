﻿namespace CarpoolingProject.Models.RequestModels
{
    public class SearchUserRequestModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CheckBlocked { get; set; }
    }
}
