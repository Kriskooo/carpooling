﻿using System;

namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateTimeOfDepartureRequestModel
    {
        public int Id { get; set; }
        public DateTime DepartureTime { get; set; }
    }
}
