﻿namespace CarpoolingProject.Models.RequestModels
{
    public class AddCitiesToCountryRequestModel
    {
        public int Id { get; set; }
        public int CityId { get; set; }
    }
}
