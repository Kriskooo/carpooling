﻿namespace CarpoolingProject.Models.RequestModels
{
    public class CreateCountryRequestModel
    {
        public string Name { get; set; }
    }
}
