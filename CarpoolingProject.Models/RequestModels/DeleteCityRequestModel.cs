﻿namespace CarpoolingProject.Models.RequestModels
{
    public class DeleteCityRequestModel
    {
        public int Id { get; set; }
    }
}
