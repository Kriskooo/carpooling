﻿namespace CarpoolingProject.Models.RequestModels
{
    public class ReviewRequestModel
    {
        public int TravelId { get; set; }
        public int Stars { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
