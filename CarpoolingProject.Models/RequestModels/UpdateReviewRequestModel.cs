﻿namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateReviewRequestModel:ReviewRequestModel
    {
        public int ReviewId { get; set; }
    }
}
