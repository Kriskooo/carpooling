﻿namespace CarpoolingProject.Models.RequestModels
{
    public class FinishedTravelRequestModel
    {
        public int Id { get; set; }
        public bool IsFinished { get; set; }
    }
}
