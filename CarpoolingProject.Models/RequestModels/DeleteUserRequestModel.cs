﻿namespace CarpoolingProject.Models.RequestModels
{
    public class DeleteUserRequestModel
    {
        public int Id { get; set; }
    }
}
