﻿namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateUserRequestModel : CreateUserRequestModel
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
    }
}
