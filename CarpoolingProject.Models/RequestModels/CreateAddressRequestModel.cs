﻿namespace CarpoolingProject.Models.RequestModels
{
    public class CreateAddressRequestModel
    {
        public string StreetName { get; set; }
        public int CityId { get; set; }

    }
}
