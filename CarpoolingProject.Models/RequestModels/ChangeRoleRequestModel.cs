﻿namespace CarpoolingProject.Models.RequestModels
{
    public class ChangeRoleRequestModel
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
