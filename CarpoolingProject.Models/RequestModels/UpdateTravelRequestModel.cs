﻿using System.ComponentModel.DataAnnotations;

namespace CarpoolingProject.Models.RequestModels
{
    public class UpdateTravelRequestModel:CreateTravelRequestModel
    {
        public int TravelId { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
