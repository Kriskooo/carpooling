﻿namespace CarpoolingProject.Models.RequestModels
{
    public class SelectPassengerRequestModel
    {
        public int TravelId { get; set; }
        public int PassengerId { get; set; }
        public bool Liked { get; set; }
    }
}
