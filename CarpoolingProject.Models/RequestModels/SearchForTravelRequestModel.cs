﻿using System;

namespace CarpoolingProject.Models.RequestModels
{
    public class SearchForTravelRequestModel
    {
        public string StartCity { get; set; }
        public string EndCity { get; set; }
        public DateTime Date { get; set; }
        
    }
}
