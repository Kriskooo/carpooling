﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarpoolingProject.Models.EntityModels
{
    public class Travel
    {
        [Key]
        public int TravelId { get; set; }
        public int CreatorId { get; set; }
        public User Creator { get; set; }
        [Required]
        public Address StartPoint { get; set; }
        public int StartPointId { get; set; }
        [Required]
        public Address EndPoint { get; set; }
        public int EndPointId { get; set; }
        [Required]
        public DateTime DepartureTime { get; set; }
        [Required]
        public int FreeSpots { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsFinished { get; set; }
        public double PricePerPeson { get; set; }
        public string AdditionalInfo { get; set; }
        public string Status { get; set; }
        public virtual ICollection<TravelPassenger> Passengers { get; set; } = new List<TravelPassenger>();
        public ICollection<TravelApplication> ApplicantsForTravel { get; set; } = new List<TravelApplication>();
    }
}
