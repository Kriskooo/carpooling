﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarpoolingProject.Models.EntityModels
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "Username must be {0}"), MaxLength(20)]
        public string UserName { get; set; }

        [Required, MinLength(8, ErrorMessage = "Password length must be at least {1} characters!")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$", ErrorMessage = "Password is invalid!")]
        public string PasswordHash { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Firstname must be at least {0} characters long"), MaxLength(20)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "Lastname must be at least {0} characters long"), MaxLength(20)]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }

        [StringLength(10, ErrorMessage = "Phone number is invalid!")]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Phone number is invalid!")]
        public string PhoneNumber { get; set; }

        public string ImagePath { get; set; }
        public int TravelCountAsDriver { get; set; }
        public int StarsCount { get; set; }
        public double AverageRating { get; set; }
        public int ReviewCount { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<UserRole> Roles { get; set; } = new List<UserRole>();
        public virtual ICollection<Travel> Travels { get; set; } = new List<Travel>();
        public virtual ICollection<TravelPassenger> PassengerForTravels { get; set; } = new List<TravelPassenger>();
        public virtual ICollection<Review> CreatedRatings { get; set; } = new List<Review>();
        public virtual ICollection<Review> RatingsForUser { get; set; } = new List<Review>();
        public virtual ICollection<TravelApplication> TravelApplications { get; set; } = new List<TravelApplication>();
    }
}
