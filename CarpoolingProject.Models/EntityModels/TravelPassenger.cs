﻿namespace CarpoolingProject.Models.EntityModels
{
    public class TravelPassenger
    {
        public int TravelId { get; set; }
        public Travel Travel { get; set; }
        public int PassengerId { get; set; }
        public User Passenger { get; set; }
        public bool IsDeclined { get; set; }
    }
}
