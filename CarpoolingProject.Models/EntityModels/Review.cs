﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarpoolingProject.Models.EntityModels
{
    public class Review
    {
        [Key]
        public int ReviewId { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int RatedUserId { get; set; }
        public User RatedUser { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastChangedOn { get; set; }
        public string Description { get; set; }
        public bool isDeleted { get; set; }
        public bool IsChanged { get; set; }
        public int StarsCount { get; set; }
    }
}
