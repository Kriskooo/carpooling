﻿namespace CarpoolingProject.Models.Dtos
{
    public class ApplicantsDto : UserDto
    {
        public ApplicantsDto()
        {

        }
        public int TravelId { get; set; }
        public string StartPoint { get; set; }
        public string EndPoint { get; set; }
    }
}
