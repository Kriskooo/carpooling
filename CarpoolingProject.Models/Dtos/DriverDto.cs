﻿
namespace CarpoolingProject.Models.Dtos
{
    public class DriverDto
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int TravelCount { get; set; }
    }
}
