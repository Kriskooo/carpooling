﻿using System;

namespace CarpoolingProject.Models.Dtos
{
    public class RatingDto
    {
        public int RatingId { get; set; }
        public int AuthorId { get; set; }
        public int RatedUserId { get; set; }
        public string Description { get; set; }
        public int StarsCount { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
