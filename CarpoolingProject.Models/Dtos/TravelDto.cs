﻿using CarpoolingProject.Models.EntityModels;
using System;
using System.Collections.Generic;

namespace CarpoolingProject.Models.Dtos
{
    public class TravelDto
    {
        public TravelDto(Travel travel)
        {
            this.TravelId = travel.TravelId;
            this.UserName = travel.Creator.UserName;
            this.AverageRating = travel.Creator.AverageRating;
            this.StartCity = travel.StartPoint.City.Name +", "+travel.StartPoint.StreetName;
            this.EndCity = travel.EndPoint.City.Name + ", " + travel.EndPoint.StreetName;
            this.DepartureTime = travel.DepartureTime;
            this.FreeSeats = travel.FreeSpots;
            this.AdditionalInfo = travel.AdditionalInfo;
            this.Price = travel.PricePerPeson;
            this.Passengers = travel.Passengers;

        }
        public int TravelId { get; set; }
        public string UserName { get; set; }
        public double AverageRating { get; set; }
        public string StartCity { get; set; }
        public string EndCity { get; set; }
        public DateTime DepartureTime { get; set; }
        public int FreeSeats { get; set; }
        public string AdditionalInfo { get; set; }
        public string Status { get; set; }
        public double Price { get; private set; }
        public ICollection<TravelPassenger> Passengers { get; set; } = new List<TravelPassenger>();
    }
}